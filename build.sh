#!/bin/bash

mkmanifest() {
  > MANIFEST
  while read -r line; do
    line=$(perl -e 'print substr shift @ARGV, 2, -10' "$line")
    [ -z "$line" ] && continue
    echo "MANIFEST > $line"
    echo "$line" >> MANIFEST
  done <<< $(find . -name README.md)
}

render() {
  if [ -z  "$1" ]; then
    echo "usage: render dir"
    exit
  fi

  declare dir="$1"
  echo "Building $dir"
  pandoc --template templates/main.html -s -o "$dir/index.html" "$dir/README.md"
}

mkmanifest

render .

while read -r line; do
  render "$line" &
done < MANIFEST
wait

