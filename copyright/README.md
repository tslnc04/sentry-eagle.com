---
title: Copyright
keywords:
  - license
  - MIT
  - CC BY 4.0
  - copyright
  - open source
---

# Copyright Information {.title}

## Licensing, acknowledgments, and more {.subtitle}

# Licensing

The various licenses for this website itself and any open source projects it may use can be found on this page.

## Website Source

The source code for this website is licensed under [MIT](https://opensource.org/licenses/MIT) unless otherwise noted.

## Content

The content for this website is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) unless otherwise noted.

# Acknowledgments

Thank you to [Material Design](https://material.io) for the generated color palettes that are used on this website.

Thank you to the [NSA](https://nsa.gov) for the name Sentry Eagle. I'm sure this site is flagged by them as a result of using the name of one of their classified projects but the upside is that most surveillance projects have cool names. Sentry Eagle is also a name used by the [Air National Guard's 173rd Fighter Wing](https://www.ang.af.mil/Media/Article-Display/Article/2041900/sentry-eagle-2020-preparation-lays-groundwork-for-exercise/) as the name of a training exercise.
