---
title: Projects
keywords:
  - projects
  - programming
  - golang
  - go
  - rust
---

# Projects {.title}

I have a lot of random ideas for projects and only some of them actually turn out well. This is a list of the projects that I think turn out well enough (that is, they're mostly finished rather than left completely unfinished) that they are public on my [GitLab](https://gitlab.com/tslnc04). Most projects that I start and never finish are still listed as private.

## Alpheratz in Rust

Alpheratz is the name I gave to the virtual machine in a programming language that I started to create about a year ago. The programming language had a functioning lexer, parser, and of course, virtual machine. I have yet to create the compiler that transforms the parse tree into the bytecode for the virtual machine.

This has been my inaugural project in my latest iteration of trying to learn Rust. Previous projects have been left unfinished due to the general frustration I experience trying to work with the compiler. However, this virtual machine manages to avoid dealing with lifetimes at all, which may be indicative of some other issue, but made the development process a lot smoother.

The original virtual machine can be found on [GitHub](https://github.com/tslnc04/andromeda/blob/master/vm.go) and the Rust implementation can be found on [GitLab](https://gitlab.com/tslnc04/alpheratz-rust).

## Supernova

One of the aforementioned unfinished projects was a parser generator based on Guido van Rossum's attempt to replace the Python parser (the new parser ships in 3.9). As a complement to my implementation of a PEG parser generator based on Van Rossum's, I created a lexer generator that has been based off of my many iterations of lexers, which originally stem from the `go-toml` lexer and the scanner implemented in Go's scanner package.

This lexer generator actually came out quite well, although my general laziness when it comes to actually making sure code works has left it without tests. Some day I'll come back to the project and write proper tests for it since it isn't a bad idea to learn how to use Go's wonderful built-in testing.

The source code for the lexer generator can be found on [GitLab](https://gitlab.com/tslnc04/supernova).

